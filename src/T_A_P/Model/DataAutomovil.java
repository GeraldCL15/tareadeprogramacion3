package T_A_P.Model;


public class DataAutomovil {
    
  

    private String Marca;
    private String Modelo;
    private int Año;
    private String Color;
    private String Chasis;
    private String Placa;
    private String TDC;//Tipo de Combustible 
    private String TDU;//Tipo de Uso (Particular, taxi o bus )
    private String Servicio;//privado o publico 
    private String Cilindraje;

    public String getCilindraje() {
        return Cilindraje;
    }

    public void setCilindraje(String Cilindraje) {
        this.Cilindraje = Cilindraje;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getAño() {
        return Año;
    }

    public void setAño(int Año) {
        this.Año = Año;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getChasis() {
        return Chasis;
    }

    public void setChasis(String Chasis) {
        this.Chasis = Chasis;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getTDC() {
        return TDC;
    }

    public void setTDC(String TDC) {
        this.TDC = TDC;
    }

    public String getTDU() {
        return TDU;
    }

    public void setTDU(String TDU) {
        this.TDU = TDU;
    }

    public String getServicio() {
        return Servicio;
    }

    public void setServicio(String servicio) {
        this.Servicio = servicio;
    }



}
